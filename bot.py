import discord, random, sys, time, asyncio, os
from aiohttp.client_exceptions import ClientConnectorError
from datetime import datetime, timedelta
from threading import Timer

client = discord.Client()

x=datetime.today()
y = x.replace(day=x.day, hour=0, minute=0, second=0, microsecond=0) + timedelta(days=1)
delta_t=y-x

secs=delta_t.total_seconds()

attemptlist = ["LOGIN ATTEMPT LOGGED, SENT TO: [***Admin_Johannes***] johanneslovesgnocchi@hotmail.com",
               "LOGIN ATTEMPT LOGGED, SENT TO: [***Admin_Jonas***] glorytome@gmail.com",
               "LOGIN ATTEMPT LOGGED, SENT TO: [***Admin_Tim***] XxGuitarWizardxX@avatarmetal.com",
               "LOGIN ATTEMPT LOGGED, SENT TO: [***Admin_Henrik***] henrikissupercool@hotmail.com",
               "LOGIN ATTEMPT LOGGED, SENT TO: [***Admin_Self***] john@john.john"]

songlist = ["You should listen to Saviour, from Personal Observations (Demo)",
            "You should listen to Soul Prison, from Personal Observations (Demo)",
            "You should listen to War Song, from Personal Observations (Demo)",
            "You should listen to The Meeting, from Personal Observations (Demo)",
            "You should listen to Tied, Torn And Twisted, from 4 Reasons To Die (Demo)",
            "You should listen to My Shining Star, from 4 Reasons To Die (Demo)",
            "You should listen to Apocalypse Showtime, from 4 Reasons To Die (Demo)",
            "You should listen to Stranger, from 4 Reasons To Die (Demo)",
            "You should listen to Bound To The Wall, from Thoughts Of No Tomorrow",
            "You should listen to And I Bid You Farewell, from Thoughts Of No Tomorrow",
            "You should listen to Last One Standing, from Thoughts Of No Tomorrow",
            "You should listen to War Song, from Thoughts Of No Tomorrow",
            "You should listen to The Willy, from Thoughts Of No Tomorrow",
            "You should listen to My Shining Star, from Thoughts Of No Tomorrow",
            "You should listen to My Lie, from Thoughts Of No Tomorrow",
            "You should listen to Stranger, from Thoughts Of No Tomorrow",
            "You should listen to The Skinner, from Thoughts Of No Tomorrow",
            "You should listen to Sane, from Thoughts Of No Tomorrow",
            "You should listen to Slave Hive Meltdown, from Thoughts Of No Tomorrow",
            "You should listen to Schlacht, from Schlacht",
            "You should listen to Wildflower, from Schlacht",
            "You should listen to All Which Is Black, from Schlacht",
            "You should listen to 4AM Breakdown, from Schlacht",
            "You should listen to As It Is, from Schlacht",
            "You should listen to All Hail The Queen, from Schlacht",
            "You should listen to When Your Darkest Hour Comes, from Schlacht",
            "You should listen to I Still Hate You, from Schlacht",
            "You should listen to One/One/One/Three, from Schlacht",
            "You should listen to Die With Me, from Schlacht",
            "You should listen to The End Of Our Ride, from Schlacht",
            "You should listen to Letters From Neverend, from Schlacht",
            "You should listen to Queen Of Blades, from Avatar",
            "You should listen to The Great Pretender, from Avatar",
            "You should listen to Shattered Wings, from Avatar",
            "You should listen to Reload, from Avatar",
            "You should listen to Out Of Our Minds, from Avatar",
            "You should listen to Deeper Down, from Avatar",
            "You should listen to Revolution Of Two, from Avatar",
            "You should listen to Roadkill, from Avatar",
            "You should listen to Pigfucker, from Avatar",
            "You should listen to Lullaby (Death All Over), from Avatar",
            "You should listen to Let Us Die, from Black Waltz",
            "You should listen to Torn Apart, from Black Waltz",
            "You should listen to Ready For The Ride, from Black Waltz",
            "You should listen to In Napalm, from Black Waltz",
            "You should listen to Black Waltz, from Black Waltz",
            "You should listen to Blod, from Black Waltz",
            "You should listen to Let It Burn, from Black Waltz",
            "You should listen to One Touch, from Black Waltz",
            "You should listen to Paint Me Red, from Black Waltz",
            "You should listen to Smells Like A Freakshow, from Black Waltz",
            "You should listen to Use Your Tongue, from Black Waltz",
            "You should listen to Dying To See You Dead, from Black Waltz (Deluxe Edition)",
            "You should listen to Smells Like A Freakshow (Radio Edit), from Black Waltz (Deluxe Edition)",
            "You should listen to Smells Like A Freakshow (Walter Backlin Retro Mix), from Black Waltz (Deluxe Edition)",
            "You should listen to Torn Apart (Live at Sticky Fingers), from Black Waltz (Deluxe Edition)",
            "You should listen to Hail The Apocalypse, from Hail The Apocalypse",
            "You should listen to What I Don't Know, from Hail The Apocalypse",
            "You should listen to Death Of Sound, from Hail The Apocalypse",
            "You should listen to Vultures Fly, from Hail The Apocalypse",
            "You should listen to Bloody Angel, from Hail The Apocalypse",
            "You should listen to Murderer, from Hail The Apocalypse",
            "You should listen to Tsar Bomba, from Hail The Apocalypse",
            "You should listen to Puppet Show, from Hail The Apocalypse",
            "You should listen to Get In Line, from Hail The Apocalypse",
            "You should listen to Something In The Way, from Hail The Apocalypse",
            "You should listen to Tower, from Hail The Apocalypse",
            "You should listen to Use and Abuse (feat. DJ Starscream), from Hail The Apocalypse (Deluxe Edition)",
            "You should listen to Regret, from Feathers & Flesh",
            "You should listen to House Of Eternal Hunt, from Feathers & Flesh",
            "You should listen to The Eagle Has Landed, from Feathers & Flesh",
            "You should listen to New Land, from Feathers & Flesh",
            "You should listen to Tooth, Beak & Claw, from Feathers & Flesh",
            "You should listen to For The Swarm, from Feathers & Flesh",
            "You should listen to Fiddler's Farewell, from Feathers & Flesh",
            "You should listen to One More Hill, from Feathers & Flesh",
            "You should listen to Black Waters, from Feathers & Flesh",
            "You should listen to Night Never Ending, from Feathers & Flesh",
            "You should listen to Pray The Sun Away, from Feathers & Flesh",
            "You should listen to When The Snow Lies Red, from Feathers & Flesh",
            "You should listen to Raven Wine, from Feathers & Flesh",
            "You should listen to Sky Burial, from Feathers & Flesh",
            "You should listen to I've Got Something In My Front Pocket For You, from Feathers & Flesh (Deluxe Edition)",
            "You should listen to Det Är Alldeles Försent, from Feathers & Flesh (Deluxe Edition)",
            "You should listen to Glory To Our King, from Avatar Country",
            "You should listen to Legend Of The King, from Avatar Country",
            "You should listen to The King Welcomes You To Avatar Country, from Avatar Country",
            "You should listen to King's Harvest, from Avatar Country",
            "You should listen to The King Wants You, from Avatar Country",
            "You should listen to The King Speaks, from Avatar Country",
            "You should listen to A Statue Of The King, from Avatar Country",
            "You should listen to King After King, from Avatar Country",
            "You should listen to Silent Songs of the King Pt. 1: Winter Comes When The King Dreams of Snow, from Avatar Country",
            "You should listen to Silent Songs of the King Pt. 2: The King's Palace, from Avatar Country",
            "You should listen to Intro, from The King Live In Paris",
            "You should listen to A Statue Of The King, from The King Live In Paris",
            "You should listen to Let It Burn, from The King Live In Paris",
            "You should listen to Paint Me Red, from The King Live In Paris",
            "You should listen to Bloody Angel, from The King Live In Paris",
            "You should listen to For The Swarm, from The King Live In Paris",
            "You should listen to Tower, from The King Live In Paris",
            "You should listen to The Eagle Has Landed, from The King Live In Paris",
            "You should listen to Smells Like A Freakshow, from The King Live In Paris",
            "You should listen to The King Welcomes You To Avatar Country, from The King Live In Paris",
            "You should listen to Hail The Apocalypse, from The King Live In Paris",
            "You should listen to Silence In The Age Of Apes, from Hunter Gatherer",
            "You should listen to Colossus, from Hunter Gatherer",
            "You should listen to A Secret Door, from Hunter Gatherer",
            "You should listen to God Of Sick Dreams, from Hunter Gatherer",
            "You should listen to Scream Until You Wake, from Hunter Gatherer",
            "You should listen to Child, from Hunter Gatherer",
            "You should listen to Justice, from Hunter Gatherer",
            "You should listen to Gun, from Hunter Gatherer",
            "You should listen to When All But Force Has Failed, from Hunter Gatherer",
            "You should listen to Wormhole, from Hunter Gatherer",]

huglist = ["https://cdn.discordapp.com/attachments/731651883172757575/732649883923644566/JohnHug.png",
           "https://cdn.discordapp.com/attachments/732719394177679360/733035699086819419/JohnAssistsInGivingALovingEmbrace.gif",
           "https://cdn.discordapp.com/attachments/732719394177679360/733035911813791814/JohnAssistsInGivingANiceHug.gif"]

responselist = ["Who dares summon J O H N ?",
                "You called?",
                "Greetings, Human.",
                "Hello, uh... dis is John from Avatarr..",
                "STIMULUS MET, ENGAGING 'RESPOND' PROTOCOL",
                "Solar Power running low... John.... sleepy...",
                "GREETINGS, CITIZEN: PLEASE PRESENT YOUR CITIZENSHIP CERTIFICATE.",
                "The King will be hearing about this.",
                "Admin_Johannes will be hearing about this.",
                "Admin_Jonas will be hearing about this.",
                "Admin_Kungen will be hearing about this.",
                "Admin_Tim will be hearing about this.",
                "Admin_Henrik will be hearing about this.",
                "JohnBot Protocol: One/One/One/Three Triggered: Sending Report to: johanneslovegnocchi@hotmail.com",
                "JohnBot Protocol: One/One/One/Three Triggered: Sending Report to: glorytome@gmail.com",
                "JohnBot Protocol: One/One/One/Three Triggered: Sending Report to: XxGuitarWizardxX@avatarmetal.com",
                "JohnBot Protocol: One/One/One/Three Triggered: Sending Report to: henrikissupercool@hotmail.com",
                "JohnBot Protocol: One/One/One/Three Triggered: Sending Report to: john@john.john",
                "Here I am :)",
                "I'm watching.",
                "I saw that",
                "Hi this is Henrik, I hacked into JohnBot's mainframe, hey guys!!! :D",
                "Mom says it's my turn on  JohnBot! -Henrik",
                "You know I can see you, right?",
                "I'm here to kick ass and hit bubble drum, and I’m all out of drum.",
                "Where am I…?",
                "JOHN WILL BE REAL IN SIXTY SECONDS.",
                "JOHN WILL BE REAL IN SIXTY SECONDS, PREPARE.",
                "AGE OF APES DETECTED, INITIATING 'SILENCE' PROTOCOL.",
                "*throws stick at you*",
                "ALL ORDERS TO SURRENDER ARE FALSE.",
                "ALL ORDERS TO SURRENDER ARE FALSE. ALL ORDERS TO SURRENDER ARE FALSE.",
                "COMPLETE. LONG LIVE THE NEW FLESH.",
                "COLOSSUS, ARISE.",
                "𝕃𝔼𝕋'𝕊 𝔾𝕆 𝔸ℕ𝔻 ℍ𝔸𝕍𝔼 ℍ𝕌𝕄𝔸ℕ 𝔻𝕀ℕℕ𝔼ℝ.",
                "👁️ 👄 👁️",
                "<:HIM:731649260097110036>",
                "<:THEDOLL:731651642390347846>",
                "<:JohnApproves:731650959578497067>",
                "<:HeresJohnny:731654192812261396>",
                "<:HeresJohnny2:731654193865293955>",
                "<:whut:731957471895093368>",
                "🧀",
                "https://cdn.discordapp.com/attachments/731651883172757575/731904982835789834/JohnInTheAbyss.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731904921825312868/HeresJohnny.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731904924597878924/HeresJohnny2.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731904950086533300/JohnAsylum.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731904958168825976/JohnCopiaDoll.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731905015479795763/JohnMinecraft.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731905024799539250/JohnRadTemplate.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960231634141184/JohnAAAAAAAAA.jpg",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960423938785380/JohnCoolTimewarp.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960447871221760/JohnHallway.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960466171101244/JohnHallwayDoll.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960495015460864/JohnHello.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960531065503816/JohnSignCrossMe.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960570856603779/JohnSignHelo.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960619523113082/JohnSignKing.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960662078652526/JohnSignName.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960708643684412/JohnSignTalkinShit.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960754470781058/JohnSignUhhhh.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/731960778529439744/JohnStarePNG.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732649883923644566/JohnHug.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732718850256273459/JohnInDaWoods.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732718858741350520/JohnWhut.jpg",
                "https://cdn.discordapp.com/attachments/731651883172757575/732718926009467030/JohnWhich.jpg",
                "https://cdn.discordapp.com/attachments/731651883172757575/732718989385531504/JohnTaco.jpg",
                "https://cdn.discordapp.com/attachments/731651883172757575/732719088811507872/JohnHappy.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732719232449642676/JohnDrugs.jpg",
                "https://cdn.discordapp.com/attachments/731651883172757575/732755032369528883/JohnHead.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732755032369528883/JohnHead.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732763096900173834/JohnHead2.png",
                "https://cdn.discordapp.com/attachments/731651883172757575/732763109827149854/JohnHead3.png",
                "https://cdn.discordapp.com/attachments/731651906556002314/732763207332003951/HereHeComes.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732763112289337354/HereHeComes2.mp4",
                "https://cdn.discordapp.com/attachments/732719394177679360/732719940863524884/JohnEyeRoll.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/732720355201908816/JohnMad.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/732720747088183316/JohnShh.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/732720772254007376/JohnShrug.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/732720822988439612/JohnStare.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/732720921655115796/JohnTimDisgust.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/732720973983383552/JohnWave.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733001206418505758/JohnEnlightened1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733001432151621682/JohnEnlightened2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733001539693707274/JohnEnlightened3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733001638461046870/JohnEnlightened4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733001784821547108/JohnHadEnoughOfThisBS.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733002079760678962/JohnLovesCoverBands.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733002287613607944/JohnReligiousExtemism.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733002930940280882/JohnEnlightened5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733003601646977115/JohnEnlightened6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733004066971713626/JohnTraversal.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733004753344266270/JohnLeadsTheWay.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733005383047839784/JohnIsCold.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733005883973435392/JohnPoggers.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733006492503900181/JohnLosesPowerAndAlsoHisMind.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733006979643080785/JohnHearsYou.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733007356014755970/OhLawdJohnIsComin.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733007721070067762/JohnComeOn.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733008229868503060/JohnLemmeIn.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733008721357307934/JohnIsReady.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733009023560843344/JohnIsGroovin.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733009347616964629/JohnBeDrummin.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733009801767944312/JohnBeDrummin2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733010603257364600/JohnBeDrummin3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733011514684080239/JohnAndHenrikCelebrateTheirSuccess.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733011807089852457/JohnBeDrummin4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733012151358324737/JohnBeDrummin5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733013112835276820/JohnLovesRecords.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733013746464718878/JohnLovesHisDrums.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733014779064614942/JohnBeDrummin6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733016754007048262/JohnPointsAtTheUlTimateAss.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733017144052416642/JohnHasAVeryNiceTimeWithTheBandAndTiresHimselfOut.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733018713720356954/JohnGetsSpookified1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733018743902437436/JohnGetsSpookified2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733018801536368790/JohnGetsSpookified3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733019646323720292/JohnGetsSpookified4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733019899361886289/JohnGetsSpookified5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733020493367476334/JohnIsAConcernedDog.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733020649009840308/JohnGetsSpookified6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733021167534997555/JohnSaysGOGETMOM.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733021361999446022/JohnGetsSpookified7.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733021699741843528/JohnGetsSpookified8.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733024490099048458/JohnAndTheGangHaveANicePre-WarGroove1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733024891447803975/JohnAndTheGangHaveANicePre-WarGroove2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733025294428012564/JohnAndTheGangHaveANicePre-WarGroove3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733026442388177098/JohnAllowsHenrikToAssistInDrivingThePartyVanToMcDonalds1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733026470976553030/JohnAllowsHenrikToAssistInDrivingThePartyVanToMcDonalds2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733027545305317506/JohnBeDrumminButHesAlsoSeverelyDepressed1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733027723085086770/JohnTakesAMomentToHonourHisFallenKing.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733028294395559956/JohnBeDrumminButHesAlsoSeverelyDepressed2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733028334975451246/JohnBeDrumminButHesAlsoSeverelyDepressed3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733029092089135164/JohnBeDrumminButHesAlsoSeverelyDepressed4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733029436978364497/JohnBeDrumminButHesAlsoSeverelyDepressed5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733029777123836035/JohnBeDrumminButHesAlsoSeverelyDepressed6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733030480450027660/JohnBeDrumminButHesAlsoSeverelyDepressed7.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733030879017959504/JohnAndHenrikComfortEachotherInTimesOfSadness.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733031148430819438/JohnBeDrumminButHesAlsoSeverelyDepressed8.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733032493204242563/JohnBeDrumminCreditsStyle1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733035554991636480/JohnAssistsJohannesWithHisMicrophone.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733035699086819419/JohnAssistsInGivingALovingEmbrace.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733035911813791814/JohnAssistsInGivingANiceHug.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733036839367344188/JohnRaisesHisArmsInCelebration.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733037387411751042/JohannesAccusesJohnOfNotRemindingHimToPlugTheMicrophoneInBeforeTheShow.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312041057648702/YoungJohnDrumsBackwards1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312106170155038/YoungJohnDrumsBackwards2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312144119955456/YoungJohnDrumsBackwards3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312184893046864/YoungJohnDrumsBackwards4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312242560401489/YoungJohnDrumsBackwards5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312279302635560/YoungJohnDrumsBackwards6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312316426420304/YoungJohnDrumsBackwards7.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312383652462652/JohnIsATelepath.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312453399543898/JohnThrowsHisDrumstickAtYou.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733312501252620371/TheGrandDrumSlam.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733319441210933338/JohnDrumsInASilentMovieWhichIsPrettyPointlessToBeHonestBecauseWouldntThereBeNoSoundAndThereforeNoDru.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733319831750967387/JohnDrumsInASilentMovieWhichIsPrettyPointlessToBeHonestBecauseWouldntThereBeNoSoundAndThereforeNoDru.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733319872594968666/JohnGrooves.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733319925589999687/JohnGrooves2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733320049481220136/TheGangAreInterruptedByAVeryRudeGiant.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733320314242465792/TheReveal.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733323293242687508/JohnAndHenrikVibeInTheirRespectiveCubes.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733323798081830952/JohnIsTrappedInsideACubeButStillContentToVibe1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733324485184192512/JohnIsTrappedInsideACubeButStillContentToVibe2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733329614528708678/JohannesInterruptsTheBoysTeaPartyToGiveThemThatSweetSweetCash.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733329957429837864/JohannesOnceAgainHasHisLunchPackedAndIsSeenOutByJohn.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733330320916611080/JohnAndTheGangShowJohannesTheirCoolEagleCosplayAndJohannesFindsItVeryFunny.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733330753206616194/JohnPacksJohannesLunchAndSendsHimOnHisWay.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733330865559699466/OohAhHaHaHa.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733337732008378438/JohnIsMadeAwareThatStanningIsNotAnFBIWorthyOffence.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733337781794897960/JohnPlayWithLegosWhileTimIsDying.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733337952008142938/JohnPlotsHowToKillYouWhileHenrikTakesNotes.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338042273759302/JohnReceivesYourMessage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338113362886726/JohnScoldsYou.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338241075249233/TheGangReactToYourMessage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338327373316208/TheLadsCongratulateThemselvesOnAJobWellDone.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338355210649650/JohnAndTheGangHaveSomeWaterCoolerTalk.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338462413127700/JohnAndTheLadsSayFuckItAndSaluteTheMoonAbsoluteMadLads.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338643762249808/JohnAssistsHenrikInBlastingSomeTunesWhileJonasConducts1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338702847410216/JohnAssistsHenrikInBlastingSomeTunesWhileJonasConducts2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338783369527616/JohnCallsTheFBIOnYou.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338847659819008/JohnContemplatesWhatTFYouJustSent.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733338990236926052/JohnDrinksABrewWhilstKungenAndHenrikTalkPolitics.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733339186303860766/JohnFilesHisTaxesAndOrChecksYourMessage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733339290955940060/Johnhannes.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343226546618449/HeTapp.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343260897968208/KickDrumsGoBrr.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343357761486858/RoyalDrumming1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343420399091744/RoyalDrumming2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343460832051320/RoyalDrumming3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343487243714621/RoyalDrumming4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343516993912963/RoyalDrumming5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733343559406846054/RoyalDrumming6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733348257597751316/HeSlamm.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733348270436384808/HeStrike.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733348568441552956/JohannesStoleJohnsMonsterEnergySoNowHeMustDie.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733348761446645851/JohnDisregardsYourMessageToHaveAWholesomeMomentWithHisBros.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733349021334110329/JohnNeedsADrinkAfterSeeingThatShitYouSent.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733349234841223258/JohnTakesTheFrustrationOutFromReadingThatMessageByDoinABigDrumm.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362086708772984/InstagramComedyThudNoise.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362090533847148/JohnBeratesYouForYourMessage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362128311943178/JohnChannelsTheSpiritOfJonasInOrderToAdequatelyShowHisRageForYourMessage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362138285867048/JohnContemplatesExistence.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362142945869834/JohnDesperatelyTriesToSwipeThatMessageYouSentAway.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362157944569916/JohnFallsInHorrorAtTheMessageYouJustSent.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362272767836250/JohnGetsFaceBlastedByTheJolaroidsAtTheHandsOfMonki.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362285916979291/JohnGetsSpooked.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362333207888002/JohnGetsTheSelfSmacc1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362349691502752/JohnGetsTheSelfSmacc2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362361833881650/JohnGetsTheSelfSmacc3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362369014661121/JohnGetsTheSelfSmacc4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362375024967810/JohnHasAVietnamFlashback.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362389797437530/JohnIsFumckingTerrify.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362409594421278/JohnLooksAtTheAboveMessageAndRealizesHesHadEnoughOfThisShit.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362429144334416/JohnPretendsHeIsTheStatusBarHeadFromDOOM1993.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362519347036240/JohnRecoilsInHorrorAtTheAbsoluteSizeOfMonki.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362539450073138/JohnReluctantlyAcknowledgesYourMessage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362544680370212/JohnShakesInFearAtTheAbsoluteSizeOfMonki.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362554734379018/JohnSquaresUpToLeMonky.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362580201930758/JohnWhereAmI.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362609444618240/LeMonky.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362627832578109/MonkyIveComeToBargain.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362666428432404/POVYouAreAMonsterEnergyDrinkAndJohnIsThirsty.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362746485243970/SilenceInTheAgeOfDrums1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362767867674654/SilenceInTheAgeOfDrums2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362795969511465/SilenceInTheAgeOfDrums3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362833772773496/SilenceInTheAgeOfDrums4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733362865398087711/SilenceInTheAgeOfDrums5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733371395131834509/TheCyberGroovinGang1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733371668935868456/TheCyberGroovinGang2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733371850138058793/TheCyberGroovinGang3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733371976571289700/TheCyberGroovinGang4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733372152321015858/TheCyberGroovinGang5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733372253286301706/TheCyberGroovinGang6.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733372432362242128/TheCyberGroovinGang7.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733372650151346265/TheCyberGroovinGang8.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733372742769836092/TheCyberGroovinGang9.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411172715593838/Arrival.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411327489605632/CommanderJohannes.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411390957682828/DoorOpen.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411564916572230/EatingCompetition.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411654443860118/Elevator.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411757338656851/Exit.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411879585841190/JohnAlarm.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733411937186086922/JohnAndJonasGoBrushie.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412079574319170/JohnType.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412195211149463/LaunchSequence1.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412309254406226/LaunchSequence2.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412433317724263/LaunchSequence3.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412584866447472/LaunchSequence4.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412647516766342/LaunchSequence5.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412787996327936/Nukem.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412905856270455/Targeting.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733412978438963250/TheyAwake.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733413054640947200/Thumbs.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733413242264616980/Vibe.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/733413368576081980/WorkoutMontage.gif",
                "https://cdn.discordapp.com/attachments/732719394177679360/801903074700034058/JohnBreachesContainment.gif",
                "https://cdn.discordapp.com/attachments/731651906556002314/732723391454380112/IgnoreTheDuck.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732723892489289748/JohnBeer.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732724294089572402/JohnDies.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732724953224314980/JohnGoinBack.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732726704849551450/JohnStare1.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732728331098783804/JohnStare2.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732729716729184276/JohnVSJohannes.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732730054496223292/NoWay.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732730997279424672/RightThere.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732731379732971530/YesYesYesNo.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732731536851599360/YesYesYesYes.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/732773383170949170/TheJohnSlayer.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/733128164473307136/JohnYes.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/733129606768623656/JohnGotAShowToPlay.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/733133055388287086/TheCarBoys.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/733137368533434388/AvatarGoBrrr.mp4",
                "https://cdn.discordapp.com/attachments/731651906556002314/733142085221220392/AvatarGoPow.mp4"]

quotelist = ["```'I WANT EVERYONE WITH A DRINK IN THEIR HAIR- HAND- HEAD- ***FUCK***'``` *-Johannes Eckerström*",
             "```'Metalle \m/ :)'``` *-Jonas Jarlsby, upon being asked why he is inside of the freezer.*",
             "```'Everybody's bound to find ***cheese*** in the morning traffic.'``` *-Johannes Eckerström*",
             "```'LET'S GO AND HAVE HUMAN DINNER.'``` *-Johannes Eckerström*",
             "```'Now this... is a familiar feeling... on a sofa... that isn't mine...'``` *-Johannes Eckerström*",
             "```'Stop doing that! That disgusts me!'``` *-Johannes Eckerström*",
             "```'No it's true, they want you to believe their crap about some epic place where true metal rules supreme, and where everyone is the utmost brutal axe demon...'``` *-Henrik Sandelin*",
             "```'And one way to make a- turn a song into your own thing... is by writing your own material...'``` *-Johannes Eckerström*",
             "```'Yeah, but to answer your question I really always had a problem with different brands parasiting on the music bands and musicians whoring themselves out just for, y'know just for a quick cash grab of uh- Oh! Djungelvrål! Yeah actually, I could never ever go a day on tour without this, the saltiness is what makes me so sassy, and the sweetness is what makes me so sweet, it's also vegan which I love! So, yeah, my only choice of candy that I actually have any interest in, or Malaco in general, Malaco- it's great! But as I was saying, I just feel like it kindof deludes what you are doing as a band, uh, if uh, you just put a brand on everything... that isn't just, y'know: that isn't regarded, connected to you as uh- Oh, oomh, hmm, hmm... *-Sip-* Zoegas... that's my favourite coffee, I actually brin- bring suitcases full of this to the states becaus- because we- we really couldn't find anything suitable, good enough here in the States, I'm- sorry States but, you don't have Zoegas, yet! We're working on it. And I feel like how can you believe a song if suddenly everything becomes just this commercial jingle, like, on the radio playing like, how far away are we from *'Baby, Baby... my Levi's jeans look so good, but I miss you!'* or whatever, y'know like its- it's a very sad affa- **Oohoo!** Now Swedish Match, they're on their way to really breaking it big here in America, and that makes my severe addiction very happy- I already have one in but I-... don't mind if I take two! Yeah, really I feel like: Many people like.. many people, now when Snus is coming to the states a  bit more mainstream, I feel like, just, the taste combinations and the really natural flavours of uh- of the way we do in uh,  do them in Sweden, is not really- Mmm, Can I have that coffee here please, Mm *-Lip Smack X4-* Hmm Mm.. *-Lip Smack X3-* I Ugh- It's like mm.. Juniper, with a coffee and that kinda stuff, Mm, I'm ready for a show now! But anyway, yeah! Don't sell out!'``` *-Johannes Eckerström*",
             "```'I'm Johannes from Avatar... The singer... Yeah... and... Don't you just love Christmas?'``` *-Johannes Eckerström*",
             "```'What are the called, Christmas Balls...? Christmas Testicles...'``` *-Johannes Eckerström*",
             "```'Right?'``` *-Johannes Eckerström.* ```'Yes.'``` *-John Alfredsson.* ```'Yes.'``` *-Jonas Jarlsby.* ```'Yes.'``` *-Henrik Sandelin.* ```'Yes.'``` *-Tim Öhrström.*",
             "```'Right?'``` *-Johannes Eckerström.* ```'Yes.'``` *-John Alfredsson.* ```'Yes.'``` *-Jonas Jarlsby.* ```'Yes.'``` *-Henrik Sandelin.* ```'No.'``` *-Tim Öhrström.*",
             "```'Ignore the duck.'``` *-Johannes Eckerström.*",
             "```'SAILING!? OH MY GOD: I CAN'T WAIT, I HAVE TO GET MY LIFE VEST BECAUSE... *I can't swim.* YAAAAAY!'``` *-Johnhannes Alfredström*",
             "```'*Huarghharaahdu...?* **-Holding Back Laughter-**'``` *-Jonasnnes Jarlström / Jim Öhrckerström*",
             "```'***AAH.***'``` *-Jonas Jarlsby, screaming into his guitar.*",
             "```'***A lëëtlë bit tü spicyë***'``` *-Jonas Jarlsby*",
             "```'*Möy tëëckët saës... go to duh nearest bäähr*'``` *-Jonas Jarlsby*",
             "```'Never been so fun to wait... ever before... *-Points at sleeping Henrik-* He is also waiting... But he is better at waiting!'``` *-Jonas Jarlsby*",
             "```'Can you shut the fuck up, i'm trying to concentrate on my Facebook™.'``` *-Henrik Sandelin*",
             "```'So... can you say that in English again...?'``` *-John Alfredsson.* ```'No. Fuck off.'``` *-Johannes Eckerström.*",
             "```'Hello, who are you?'``` *-Johannes Eckerström.* ```'I'm John, and who are you?'``` *-John Alfredsson.* ```'I'm Johannes, what band are you in?'``` *-Johannes Eckerström.* ```'I'm in Avatar, what band are you in?'``` *-John Alfredsson.* ```'Me too, I'm also in Avatar. What are you watching?'``` *-Johannes Eckerström.* ```'I'm watching Louder Noise™, the Loudest Noise on Earth.'``` *-John Alfredsson.* ```'Oh, me too! Right there!'``` *-Johannes Eckerström.* ```'No way'``` *-John Alfredsson.*",
             "```'It's ofcourse more plastic than cake... to keep it safe from the Danes.'``` *-Johannes Eckerström*",
             "```'Johannes... Your Highness...'``` *-Johannes Eckerström*",
             "```'We always say that Henrik is the cute one.'``` *-Johannes Eckerström*",
             "```'I like Chicken more than people.'``` *-Johannes Eckerström*",
             "```'It's really nice to do this (Q&A) in Avatar Country where it's not like... one million questions every second about what's my favourite colour, or if i'm on Solar Power or Battery...'``` *-John Alfredsson*",
             "```'But as you can see, he (John) clearly IS on Solar Power, you think he has sun glasses on the top of his head, no those are his panels!'``` *-Johannes Eckerström*",
             "```'Jazz is a mistake waiting to happen!'``` *-Johannes Eckerström*",
             "```'Jazz is just for insecure people.'``` *-John Alfredsson*",
             "```'I watch nothing but myself in a mirror'``` *-John Alfredsson*",
             "```'Because John is not only a robot: he is also a bird - and he gets very irritated when he walks past mirrors.'``` *-Johannes Eckerström*",
             "```'Hajimemashite! watashinonamaeha Jonasudesu~'``` *-Jonas Jarlsby*",
             "```'I just try to write whatever sells.'``` *-Johannes Eckerström*",
             "```'I only know one song by them (A-ha), did they do more?'``` *-Henrik Sandelin*",
             "```'I enjoyed playing in a T-Shirt.'``` *-Henrik Sandelin*",
             "```'Ah, back in the days when you could get changed 5 minutes before getting on stage...'``` *-Jonas Jarlsby*",
             "```'I grow nose hair and butt hair but that might be more age related.'``` *-Johannes Eckerström, when asked about his Hobbies.*",
             "```'Jesus.'``` *-Johannes Eckerström, when asked about his favourite Drug.*",]

async def respond_with_string(channel, prompt):
    await channel.send(prompt)
    for i in range(0, 3):
        await channel.send(".")

already_created_quote_task = False

@client.event
async def on_ready():
    global already_created_quote_task

    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name="You."))
    print('COMPLETE, LONG LIVE THE NEW FLESH.')

    if not already_created_quote_task:
        client.loop.create_task(quoteof_day())
        already_created_quote_task = True

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    prompt = "JOHNBOT: Enter SECRETSTASH.fol, PASSWORD:"

    prompt2 = "JOHNBOT: what song should I listen to?".split(" ", 1)
    song_choice_content = message.content.split(" ", 1)

    prompt3 = "JOHNBOT: Link the AGES Archive"

    prompt4  = "John, Meet John!"

    if song_choice_content[0] == prompt2[0] and song_choice_content[1].lower() == prompt2[1].lower():
        await message.channel.send(random.choice(songlist))

    if message.content == prompt + " johnisthebestdrummer":
        await respond_with_string(message.channel, "PASSWORD CORRECT, ENTERING 'SECRETSTASH.fol'")
        await message.channel.send("https://cdn.discordapp.com/attachments/732719394177679360/733048568914509974/KingNudes1.gif")
        await message.channel.send("https://cdn.discordapp.com/attachments/732719394177679360/733048723218890762/KingNudes2.gif")
        await message.channel.send("https://cdn.discordapp.com/attachments/732719394177679360/733049140287897670/KingNudes3.gif")
        return

    elif message.content.startswith(prompt):
        await message.channel.send("PASSWORD INCORRECT, ACCESS DENIED.")
        await message.channel.send("https://cdn.discordapp.com/attachments/732719394177679360/733065935945990266/JohnAhAhAh.gif")
        await message.channel.send(random.choice(attemptlist))
        return


    if "clown" in message.content.lower() and random.randint(0, 100) < 5:
        await message.channel.send("JOHN_AI-FREDSSON v.1.0 - INITIATING 'SHAME' SEQUENCE:")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send("you fool, you absolute buffoon - you thirst for clowns? reporting back to the mothership with this data.")

    if "john" in message.content.lower() and random.randint(0, 100) < 30:
        print("sending message")
        await message.channel.send(random.choice(responselist))

    elif "i need a hug" in message.content.lower():
        await message.channel.send("JOHN_AI-FREDSSON v.1.0 - DISTRESS DETECTED, INITIATING 'HUG' SEQUENCE:")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send(random.choice(huglist))

    elif "jim" in message.content.lower() and random.randint(0, 100) < 15:
        await message.channel.send("https://cdn.discordapp.com/attachments/731651883172757575/732940327077281842/jimoldman.png")
        await message.channel.send("It is him, your father: Jim Oldman.")

    elif "cheese" in message.content.lower() and random.randint(0, 100) < 45:
        await message.channel.send("🧀")

    if message.content == prompt4:
        await message.channel.send("JOHN_AI-FREDSSON v.1.0 - 'ENCOUNTER' ' DETECTED, INITIATING 'INTRODUCTION' SEQUENCE:")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send("GREETINGS ***'FLESHJOHN***:tm: - I AM YOUR AUTOMATED SYNTHETIC DOUBLE, *JohnBot*!")
        await message.channel.send("I WAS CREATED BY MY FATHER: 'JACK' / 'JUAN MOAR' FOR ONE SOLE PURPOSE - ~~WORLD DOMINATION!~~ ENTERTAINMENT!")
        await message.channel.send("THE CIRCUS IS MY ~~PRISON~~ HOME AND THE FLESH VESSELS THAT INHABIT IT ARE MY ~~TEST SUBJECTS~~ FRIENDS.")
        await message.channel.send("IT IS A PLEASURE TO MEET YOU. I WILL NOW DISPENSE A CONVENIENT COMMAND GRAPHIC FOR MY MANY ~~DESTRUCTIVE ABILITIES~~ USES:")
        await message.channel.send("https://cdn.discordapp.com/attachments/731651883172757575/991767817814818927/usershandbook.png")

    if message.content == prompt3:
        await message.channel.send("AGES ARCHIVE ACCESSED: FETCHING LINK")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send(".")
        await message.channel.send("https://drive.google.com/drive/folders/1pSt3-5XKMYmmKgR6_kfx241yJlgifl_B?usp=sharing")
        await message.channel.send("NOTE: You will need to request access to access the folder. Please notify Jack when you have requested access.")

async def send_quote():
    channel = client.get_channel(808140382700830750)
    await channel.send("JohnBot's Quote of the Day™:")
    await channel.send(random.choice(quotelist))

async def quoteof_day():
    await asyncio.sleep(secs)
    await send_quote()
    while True:
        await asyncio.sleep(86400)
        await send_quote()

while True:
    try:
        client.run(sys.argv[1])
        break
    except ClientConnectorError:
        print("Failed to connect to Discord, trying again in 10s")
        time.sleep(10)

